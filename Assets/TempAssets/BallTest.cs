﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class BallTest : MonoBehaviour {
    float speed=0;
    public float kConst,maxStretchZ,minStretchZ, maxStretchX, minStretchX, inpMultiplier,yAngleMultiplier;
    public Transform restPos;
    public bool bCanStretch=true;
    public float rotMultiplier = 2f;
    Vector3 dir;
    Rigidbody rb;
    Vector3 rubberStretch;
    float angle;
    bool bIsPressed = false;
    float stretchInput, finalYRot;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
   
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float finalStretchValZ = inpMultiplier * v*kConst + restPos.position.z;
        float finalStretchValX = inpMultiplier * h*kConst+restPos.position.x;
        SetSlingRot();
        //if (InputTest.instance.bShoot) {
        if (bCanStretch) { 
            float finalZ=Mathf.Clamp(finalStretchValZ, minStretchZ, maxStretchZ);
            float finalX = Mathf.Clamp(finalStretchValX, minStretchX, maxStretchX);
            finalYRot = -h * yAngleMultiplier;
            //print("finalZ:"+finalZ);
            Vector3 newPos =new Vector3(finalX, transform.position.y, finalZ);
            transform.position=Vector3.Lerp(transform.position, newPos,.2f);
            transform.eulerAngles=new Vector3(transform.eulerAngles.x,-h*yAngleMultiplier,transform.eulerAngles.z);
            //transform.Translate(new );
        }
        

        if (InputManager.instance.bCanShoot == true)
        {
            //Shoot();
        }
	}

    void Shoot()
    {
        bCanStretch = false;
        Vector3 angle = ComputeAngle();
        rb.velocity =GetLaunchVelocity();//please set new direction
        transform.eulerAngles = new Vector3(angle.x, finalYRot, angle.z);
        rb.useGravity = true;
        InputManager.instance.bCanShoot = false;
        Destroy(gameObject, 5);
    }

    public Vector3 GetLaunchVelocity()
    {
       
     
        rubberStretch = restPos.transform.position - gameObject.transform.position;
        Vector3 u = rubberStretch * Mathf.Sqrt((kConst/ rb.mass));//velocity

        return -u;
    }
    /// <summary>
    /// Finds the angle between the rest position and current position of slingshot streched.
    /// </summary>
    /// <returns></returns>
    public Vector3 ComputeAngle()
    {
        Vector3 dir = restPos.position - transform.position;
        Debug.DrawLine(transform.position, dir);
        //Quaternion finalAngle= Quaternion.Euler(dir);
        return dir;
        //float aX = Mathf.Atan2(Mathf.Sqrt((pos.y * pos.y) + (pos.z * pos.z)));
    }

    //private void OnMouseDown()
    //{
    //    bIsPressed = true;
    //}

    //private void OnMouseDrag()
    //{
    //    Input.
    //}
    //private void OnMouseUp()
    //{
    //    if (bIsPressed)
    //    {
    //        dir = restPos.position -transform.position;
    //        float newVel = SetVelocity();
    //      rb.velocity=dir.normalized*newVel;
    //        print(rb.velocity);
    //    }
    //}

    public void SetSlingRot()
    {
      float r= CrossPlatformInputManager.GetAxis("RotateAxis");

        gameObject.transform.eulerAngles = new Vector3(Mathf.Clamp(r*rotMultiplier,-50f,50f), gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z);
    }


}
