﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingTest : MonoBehaviour {
    public float  kVal=0,stretchVal, inputModifier=5;
    public Transform restPosition;

    Vector3 initialPos;
    float stoneVel, rubberStretch;
    Rigidbody rb;
    bool check = true;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
       
        float v = Input.GetAxis("Vertical");
        rubberStretch = v * stretchVal;
        Vector3 rubberstretchval = SetDirection();
        stoneVel = rubberstretchval.magnitude*v*inputModifier * Mathf.Sqrt(kVal / rb.mass);

        //rubberStretch = initialPos.magnitude - Input.mousePosition.magnitude;
        print(0.5*rb.mass*rb.velocity.magnitude);

    }

    private void OnMouseDrag()
    {
        if (check)
        {
            initialPos = Input.mousePosition;
            check = false;
        }
      }

    public Vector3 SetDirection() {
        Vector3 dir = restPosition.position - transform.position;
        return dir;
    }



}
