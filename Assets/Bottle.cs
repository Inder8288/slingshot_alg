﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bottle : MonoBehaviour {

    
    public GameObject origBottle,fractureMesh;
    public Transform newPos;


    public void BreakBottle()
    {
        origBottle.SetActive(false);
        GameObject frac = Instantiate(fractureMesh) as GameObject;
        frac.transform.SetParent(gameObject.transform);
        Destroy(frac, 3);
        Invoke("Respawn", 4);
    }

    public void Respawn()
    {
        origBottle.SetActive(true);

    }
}
