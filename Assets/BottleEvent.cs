﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleEvent : MonoBehaviour {

    Bottle bottleIns;
    private void Start()
    {
        bottleIns = gameObject.transform.GetComponentInParent<Bottle>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
            bottleIns.BreakBottle();
        }
    }

}
