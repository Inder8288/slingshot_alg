﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {
    public MainMenu mainMenu;
    public LevelSelection levelSelection;
    #region Singleton
    public static UIManager instance;
    private void Awake()
    {
        instance = this;
        
    }
    #endregion

}
