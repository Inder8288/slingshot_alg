﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {
    public void OpenLevelSelection()
    {
        UIManager.instance.levelSelection.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
	
    public void OpenSettings()
    {

    }
    
    public void LikeApp()
    {

    }
    
    public void Exit()
    {
        Application.Quit();
    }

    public void MoreGames()
    {

    }

}
