﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelSelection : MonoBehaviour {

    public void LoadLevel(int level)
    {
        LevelManager.instance.LoadLevel(level);
    }

    public void BackToMenu()
    {
        UIManager.instance.mainMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
