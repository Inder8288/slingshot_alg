﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals : MonoBehaviour {
    public Transform restPos,slingBody;
    public GameObject ballPrefab;
    public SlingShot slingShot;
    public static Transform ballThrower;
    
    #region Singleton

    public static Globals instance;
    private void Awake()
    {
        instance = this;
    } 
    #endregion
}
