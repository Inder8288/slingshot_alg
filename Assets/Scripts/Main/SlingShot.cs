﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class SlingShot : MonoBehaviour {
  [Tooltip("Assign Bone Transforms")]
    public  Transform centerLeft, centerRight,rotateWheel,spawnPoint; //main center has different transforms
    public Joystick mainJoystick;
    public GameObject ballPrefab,newBall;
    [Header("Stretch")]
    public float rubberElasticity;
    public float maxStretchV, minStretchV, lMaxStretchH, lMinStretchH, rMaxStretchH, rMinStretchH, yAngleMultiplier;
    [Header("Rotation")]
    [Tooltip("Decrease to get slow Input")]
    public float xAngleMul = 0.8f;
    [Tooltip("Decrease to get slow Input")]
    public float yAngleMul = 0.8f;
    public float minXAngle, maxXAngle, minYAngle, maxYAngle;
    [HideInInspector]
    public bool bCanStretch = true;
    [Header("Ball/Projectile")]
    public float waitToRespawn = 2f;
    public float speedModifier = 5f;

    Rigidbody ballRb;
   
    float rot;
    Vector3 rubberStretch;
    Coroutine ballRoutine;


    void Start()
    {
        newBall = Instantiate(ballPrefab) as GameObject;
        newBall.transform.SetParent(centerLeft);
        newBall.transform.position = spawnPoint.transform.position;
        ballRb = newBall.GetComponent<Rigidbody>();
        ballRb.isKinematic = true;

    }

    private void FixedUpdate()
    {
        
        Globals.ballThrower = centerLeft;
        StretchRubber();
        if (InputManager.instance.bCanShoot)
        {
            Shoot();
        }
    }

    void StretchRubber()
    {
        
        float ZstretchVal =InputManager.verAxis* rubberElasticity;
       
        SetSlingRot();
        if (bCanStretch)
        {
            float slingFinalZ = Mathf.Clamp(ZstretchVal, minStretchV, maxStretchV);
            centerLeft.localPosition = Vector3.Lerp(centerLeft.localPosition, new Vector3(centerLeft.localPosition.x, centerLeft.localPosition.y, slingFinalZ), .5f);
            centerRight.localPosition = Vector3.Lerp(centerRight.localPosition, new Vector3(centerRight.localPosition.x, centerRight.localPosition.y, slingFinalZ), .5f);

        }
    }
    
    void Shoot()
    {
        print("shoot called");
        InputManager.instance.bCanShoot = false;
        InputManager.instance.bAllowInput = false;
        newBall.transform.SetParent(null);
        ballRb.velocity = GetLaunchVelocity();
        ballRb.isKinematic = false;
        ballRb.useGravity = true;
        Destroy(newBall, waitToRespawn);
        ballRoutine = StartCoroutine(SpawnNewBall());
        ResetRotation();
    }

    public Vector3 GetLaunchVelocity()
    {

        if (newBall != null)
        {
            rubberStretch = Globals.instance.restPos.transform.position - newBall.transform.position;
            Vector3 u = rubberStretch*Mathf.Sqrt(rubberElasticity/ballRb.mass)*speedModifier ;//velocity
            return u;
        }
        else
        {
            return Vector3.zero;
        }
    }
    
    public void SetSlingRot()
    {
        Transform parent = transform.parent;
        Vector3 currentRot = parent.transform.eulerAngles;
        if (InputManager.instance.bClickedWheel)
        {
            float r = CrossPlatformInputManager.GetAxis("RotateAxis");
            if (r > 0 || r<0)
            {
                rot = r*xAngleMul;
            }
            currentRot.x = Mathf.Clamp(rot+r, minXAngle, maxXAngle);
        }
        currentRot.y= Mathf.Clamp(-InputManager.horAxis * yAngleMul, minYAngle, maxYAngle);
        parent.transform.rotation = Quaternion.Euler(currentRot);
        rotateWheel.rotation = parent.rotation;
    }
    void ResetRotation()
    {

        Transform parent = transform.parent;
        rot = 0;
        parent.transform.eulerAngles = Vector3.zero;
        
    }
    IEnumerator SpawnNewBall()
    {
        yield return new WaitForSeconds(waitToRespawn);
        newBall = Instantiate(Globals.instance.ballPrefab, spawnPoint.position, Quaternion.identity) as GameObject;
        newBall.transform.SetParent(centerLeft);
        ballRb = newBall.GetComponent<Rigidbody>();
        ballRb.isKinematic = true;
        bCanStretch = true;
        InputManager.instance.bAllowInput = true;
        InputManager.instance.clickedBall = false;
        StopCoroutine(ballRoutine);
    }


}
