﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class InputManager : MonoBehaviour {

    [Header("SlingShot Controls")]
  


   
    public static float horAxis, verAxis;
    [HideInInspector] public  bool bCanShoot=false,bIsStrecthing=false,bAllowInput=true, clickedBall=false, bClickedWheel=false;
    Touch mainTouch;

    #region Singleton
    public static InputManager instance;
    private void Awake()
    {
        instance = this;
    } 
    #endregion
	void FixedUpdate () {


        GetUserInput();
        if (!bAllowInput)
        {
            DenyUserInput();
        }
        if (Input.touchCount > 0) {
            mainTouch = Input.GetTouch(0);
            CheckPlayerTouch();
      

      
            if ((mainTouch.phase == TouchPhase.Moved||mainTouch.phase == TouchPhase.Stationary) && bAllowInput &&clickedBall)
            {
            
                bIsStrecthing = true;
            }
            else
            {
                bIsStrecthing = false;
            }
            if (mainTouch.phase == TouchPhase.Ended  && clickedBall) 
            {
                print("phase end");
                bCanShoot = true;
                bIsStrecthing = false;
             
            }
        }
    }
    void GetUserInput()
    {
       verAxis = CrossPlatformInputManager.GetAxis("Vertical");
       horAxis = CrossPlatformInputManager.GetAxis("Horizontal");
        if (verAxis > 0)
        {
            verAxis = 0;
        }    
    }


    public static void DenyUserInput()
    {
        if (verAxis > 0 || verAxis < 0)
        {
           verAxis = 0;
        }
    }

    void CheckPlayerTouch()
    {

        Ray ray=Camera.main.ScreenPointToRay(mainTouch.position);
        RaycastHit raycastHit;
           if(Physics.Raycast(ray,out raycastHit))
        {

            if (raycastHit.collider.tag == "Ball")
            {
                clickedBall = true;
            }

            if(raycastHit.collider.tag=="RotateWheel")
            {
                bClickedWheel = true;
            }
        }


    }

}
