﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour {
    public GameObject[] trajectoryPoints;
    public float time = .01f;  

	void Update () {
        if (!InputManager.instance.bCanShoot)
        {
            ResetTrajectory();
        }
        if (InputManager.instance.bIsStrecthing && InputManager.instance.bAllowInput)
        {
            DrawTrajectory();
        }
    }

    void DrawTrajectory()
    {
        ResetTrajectory();
        Vector3 ballLPos = Globals.instance.slingShot.newBall.transform.localPosition;
        Vector3 restP = Globals.instance.restPos.transform.position;
        Vector3 pos1 =new Vector3(restP.x,restP.y,ballLPos.z);
        Vector3 predictedVel = Globals.instance.slingShot.GetLaunchVelocity();
        
        for (int step = 0; step <trajectoryPoints.Length; step ++)
        {
            Vector3 pos2 = pos1 + predictedVel * time;
            trajectoryPoints[step].transform.position = pos2;
            predictedVel += Physics.gravity * time;
            pos1 = pos2;
        }
    }

    void ResetTrajectory()
    {
        for (int i = 0; i < trajectoryPoints.Length; i++)
        {
            trajectoryPoints[i].transform.position = new Vector3(0,0,-10);

        }
    }


}
